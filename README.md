# Snake with JavaScript and canvas. (OOP)



## to start the game
This snake game has been deploymented with CI/CD and its availabe here: [click here](https://schindar.gitlab.io/-/snakeoop/-/jobs/369267661/artifacts/public/index.html)

## License
[Schindar Ali](https://www.linkedin.com/in/schindar/) 


## Photo of the game

![Snake_OOP](/uploads/aeb75782eafab8c2b21d4a8641432a39/Screenshot_2019-12-04_15-17-59.png)