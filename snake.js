
class Rectangle{
    constructor (x,y,direction,gridSize){   
    this.x = x;
    this.y= y;
    this.direction = direction;
    this.gridSize = gridSize;   
    }     
    }   
    class Game {       
        constructor(canvasId, width, height,key) {

            this.stop = 100
            this.canvas = document.getElementById('myCanvas');
            this.context = this.canvas.getContext('2d');

            this.width = width;
            this.height = height;           
            this.rectangle = new Rectangle(0, 0, this.direction, 60);
        this.snake = new Snake('east',width,height);
        this.apple = new Apple(this.context,5,3,width,height);
    this.frameCount = 0;
            this.fps, this.frameTime, this.startTime, this.now, this.then;            
            this.startAnimating(3);            

            }
drawgrid(){    
    const h = this.height;
        const w = this.width;
    
        for (let i = 0; i <= w; i++) {
            for (let j = 0; j <= h; j++) {
                // drawing the grid
    
                this.drawLine(i * this.context.canvas.height / w, 0, i * this.context.canvas.height / w, this.context.canvas.height, this.context);
                this.drawLine(0, i * this.context.canvas.height / h, this.context.canvas.width, i * this.context.canvas.height / h, this.context);
                // checking the seqment position
            }
        }
        function convert(wert) {
            return wert * this.context.canvas.height / h;
        }
    }   
     drawLine(x1, y1, x2, y2, context) {
        context.beginPath();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();
    }

        update() {
            this.snake.UpdateSnake(this.context);
            if(this.snake.gameover()||this.snake.collision()){
                console.log("trueueueuueueu")
                this.stop=0    
                this.key = true
        }
        if(this.snake.check_Contact_with_apple(this.apple)){
            this.snake.addsegment(this.apple.GetAppleX(),this.apple.GetAppleY())
            this.snake.find_free_place_for_food(this.context,this.apple) 
            this.stop +=100               
                        }   
        }       
        draw() {
            this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

            this.drawgrid()

            this.snake.drawsnake(this.context);
            this.apple.drawapple(this.context)

        }             
        startAnimating(targetFPS){
            this.frameTime = 1000 / targetFPS;
            this.then = window.performance.now();
            this.startTime = this.then;
            this.frameCount = 0;
            this.animate(this.then);
        }        
        animate(currentTime) {
            if (this.frameCount <this.stop ) {
                window.requestAnimationFrame(this.animate.bind(this));
            }
            
           this.now = currentTime;
          const elapsed = this.now - this.then;
    
          if (elapsed > this.frameTime) {
    
            this.then = this.now;
    this.frameCount++
            this.update();
           if(!this.key){
               
            this.draw();
           }
                  
          }
        }
        
        
    }   
    class Snake{
constructor(directionSnake,width,height){
    document.addEventListener('keyup',this.changeDirection.bind(this))
this.width= width
this.height = height
 this.snake=[]
this.snake.push(new Rectangle(1,2,this.direction,60))
this.snake.push(new Rectangle(1,3,this.direction,60))
this.snake.push(new Rectangle(2,3,this.direction,60))
this.snake.push(new Rectangle(3,3,this.direction,60))
this.directionSnake = directionSnake
}

drawsnake(context) {


    for( let i = 0; i < this.snake.length; i ++){
        // drawing the head of snake  
        context.fillStyle = 'black';
        context.fillRect( this.snake[i].x * context.canvas.height/this.height, this.snake[i].y * context.canvas.width/this.width ,context.canvas.width/this.width-1.5,context.canvas.height/this.height-1.5);
//  drawing the head      
            context.fillStyle = 'yellow';
            context.fillRect( this.snake[0].x * context.canvas.height/this.height, this.snake[0].y * context.canvas.width/this.width ,context.canvas.width/this.width-1.5,context.canvas.height/this.height-1.5);
                    
        
    
        
    }

    
}
addsegment(x,y){
    this.snake.push(new Rectangle(x,y,this.direction,60))
}
// moving the head of snake
UpdateSnake() {
this.move_body_of_snake()


    switch(this.directionSnake)
{

case 'east':

this.snake[0].x +=1

break;
    case'west':
    this.snake[0].x -=1
    
    break;
    case'north':
    this.snake[0].y -=1
    
    break;
    case'south':
    this.snake[0].y +=1

} 
 
}

find_free_place_for_food(context,apple) {

    var foodx = apple.reandom_position_of_food()
    var foody = apple.reandom_position_of_food()
console.log("foood x " + foodx + "fooooody" +foody)
    if (this.match(this.snake, foodx,foody)) {
console.log("000000000000")
      this.find_free_place_for_food(context,apple)

    } else {

        apple.positionApdate(foodx,foody)

        console.log(apple.valuex)

        console.log(apple.valuey)
    
}

}

match(arr, v1, v2) {

    for (let i = 0; i < arr.length; i++) {
        if (v1 === arr[i].x && v2 === arr[i].y) {
            console.log("v1  " + v1+" xxxxxx  " + arr[i].x  + "v2  " + v2 + "  yyyyyy "  + arr[i].y )
            return true
        }
    }
    return false

}
collision  () {

    for (var i = 2; i < this.snake.length; i++) {
        if (
            this.headx() === this.snake[i].x &&
            this.heady() === this.snake[i].y
        ) {

            return true;
        }
    }
    console.log("faaaaaaaaa")
    return false;
}
// moving the body of the snake

 move_body_of_snake() {
    // from the back to the front
    for (let i = this.snake.length - 1; i >= 1; i--) {

        this.snake[i]['x'] = this.snake[i - 1].x
        this.snake[i]['y'] = this.snake[i - 1].y

    }

}

changeDirection(event){
    
    if(event.key==='ArrowRight'){
this.directionSnake = "east"

    }else if(event.key==='ArrowLeft'){
this.directionSnake = "west"

    }else if(event.key === 'ArrowUp'){
this.directionSnake = "north"

    }else if(event.key === 'ArrowDown'){
this.directionSnake = "south"
        
            }
  
  console.log('Key ' + event.key + ' was pressed.'); 
  
}
headx(){
return this.snake[0].x
}
heady(){
return this.snake[0].y
}
 gameover() {
     console.log(this.headx() + " üüüüüü "+ this.width)
    if (this.headx() === this.width || this.heady() === this.height || this.headx() <= -1 || this.heady() <= -1) {
       
        return true
    }
    return false
}

 check_Contact_with_apple(apple) {
    if ((apple.GetAppleX() === this.headx()) && (apple.GetAppleY() === this.heady())) {
        return true
    }
    return false
}


    }
    
    ////////////////////////////////

class Apple{
    constructor(context,valuex,valuey,width,height){
        this.width=width
        this.height = height
this.context = context
this.valuex= valuex
this.valuey = valuey
this.food =  new Rectangle(valuex,valuey,this.direction,60)
}
   drawapple(context){
    context.fillStyle = 'red';
    context.fillRect( this.food.x * context.canvas.height/this.height, this.food.y * context.canvas.width/this.width ,context.canvas.width/this.width-1.5,context.canvas.height/this.height-1.5);
       

   } 

   GetAppleX(){
       return this.food.x;
   }
   GetAppleY(){
       return this.food.y;
   }
   
 positionApdate(x,y){
this.food['x'] =x
this.food['y'] = y 
 }
    reandom_position_of_food() {

    const randomnumber = Math.floor((Math.random() * this.width-1) + 1)
    return randomnumber

}

}


    this.game =  new Game('myCanvas',8,8,false)
    
    
    
    
    
    
    
    